package grpcClient

import (
	"costomer_service/config"
    "google.golang.org/grpc"
    "fmt"

	pbp "costomer_service/genproto/post"
	rs "costomer_service/genproto/review"
)

// GrpcClientI ...
type GrpcClientI interface {
	Post() pbp.PostServiceClient
	Review() rs.ReviewServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg           config.Config
	postService   pbp.PostServiceClient
	reviewService rs.ReviewServiceClient
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {
	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.ReviewServiceHost, cfg.ReviewServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("review service dial host:%s, port: %d", cfg.ReviewServiceHost, cfg.ReviewServicePort)
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("post service dial host:%s, port: %d", cfg.PostServiceHost, cfg.PostServicePort)
	}
	return &GrpcClient{
		cfg:           cfg,
		postService:   pbp.NewPostServiceClient(connPost),
		reviewService: rs.NewReviewServiceClient(connReview),
	}, nil
}

func (s *GrpcClient) Review() rs.ReviewServiceClient {
	return s.reviewService
}
func (s *GrpcClient) Post() pbp.PostServiceClient {
	return s.postService
}
