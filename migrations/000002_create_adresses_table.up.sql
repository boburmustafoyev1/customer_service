CREATE TABLE IF NOT EXISTS addresses(
    id serial primary key,
    owner_id int references customers(id),
    country varchar(50),
    street varchar(30)
);
