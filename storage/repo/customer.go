package repo

import (
	pb "costomer_service/genproto/customer"
)

// CustomerStorageI ...
type CustomerStorageI interface {
	Create(*pb.CustomerRequest) (*pb.CustomerResponse, error)
	GetCustomerInfo(*pb.CustomerID) (*pb.CustomerInfo, error)
	UpdateCustomer(*pb.CustomerUp) (*pb.CustomerResponse, error)
	DeleteCustomer(*pb.CustomerID) (*pb.Empty, error)
}
