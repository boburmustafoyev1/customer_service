package storage

import (
	"costomer_service/storage/postgres"
	"costomer_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

// IStorage ...
type IStorage interface {
	Customer() repo.CustomerStorageI
}

type storagePg struct {
	db           *sqlx.DB
	customerRepo repo.CustomerStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:           db,
		customerRepo: postgres.NewCustomerRepo(db),
	}
}

func (s storagePg) Customer() repo.CustomerStorageI {
	return s.customerRepo
}
