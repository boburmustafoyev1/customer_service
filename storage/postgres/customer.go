package postgres

import (
	pb "costomer_service/genproto/customer"

	"fmt"

	"github.com/jmoiron/sqlx"
)

type customerRepo struct {
	db *sqlx.DB
}

// NewCustomerRepo ...
func NewCustomerRepo(db *sqlx.DB) *customerRepo {
	return &customerRepo{db: db}
}

func (r *customerRepo) Create(user *pb.CustomerRequest) (*pb.CustomerResponse, error) {
	tr, _ := r.db.Begin()
	defer tr.Rollback()
	respCustom := pb.CustomerResponse{}
	err := tr.QueryRow(`insert into customers(
		first_name, 
		last_name, 
		bio, email, 
		phone_number) values($1, $2, $3, $4, $5) returning 
		id, first_name, last_name, bio, email, phone_number, created_at`,
		user.FirstName, user.LastName, user.Bio, user.Email, user.PhoneNumber,
	).Scan(
		&respCustom.Id,
		&respCustom.FirstName,
		&respCustom.LastName,
		&respCustom.Bio,
		&respCustom.Email,
		&respCustom.PhoneNumber,
		&respCustom.CreatedAt,
	)
	if err != nil {
		tr.Rollback()
		fmt.Println("error while inserting customers")
		return &pb.CustomerResponse{}, err
	}
	var addresses []*pb.Address
	for _, address := range user.Addresses {
		addresResp := pb.Address{}
		err := tr.QueryRow(`insert into addresses(
			owner_id, 
			country, 
			street) 
			values($1, $2, $3) 
			returning 
			id, owner_id, 
			country, street`,
			respCustom.Id,
			address.Country,
			address.Street).Scan(
			&addresResp.Id,
			&addresResp.OwnerId,
			&addresResp.Country,
			&addresResp.Street)
		if err != nil {
			tr.Rollback()
			fmt.Println("error while inserting addresses")
			return &pb.CustomerResponse{}, err
		}
		addresses = append(addresses, &addresResp)
	}
	respCustom.Addresses = addresses
	if err = tr.Commit(); err != nil {
		fmt.Println("error tr.commit", err)
	}
	return &respCustom, nil
}

// GetCustomerInfo ...
func (r *customerRepo) GetCustomerInfo(req *pb.CustomerID) (*pb.CustomerInfo, error) {
	customer := pb.CustomerInfo{}
	err := r.db.QueryRow(`select 
		id, 
		first_name, 
		last_name, bio, 
		email, phone_number, created_at, 
		updated_at from customers where id=$1 and deleted_at is null`, req.Id).Scan(
		&customer.Id, &customer.FirstName, &customer.LastName,
		&customer.Bio, &customer.Email, &customer.PhoneNumber, &customer.CreatedAt, &customer.UpdatedAt,
	)
	if err != nil {
		fmt.Println("error while selecting customers")
		return &pb.CustomerInfo{}, err
	}
	rows, err := r.db.Query(`select id, owner_id, country, street from addresses where owner_id = $1`, req.Id)
	if err != nil {
		fmt.Println("error while selecting address")
		return &pb.CustomerInfo{}, err
	}
	defer rows.Close()

	for rows.Next() {
		address := pb.Address{}
		err = rows.Scan(
			&address.Id,
			&address.OwnerId,
			&address.Country,
			&address.Street,
		)
		if err != nil {
			fmt.Println("error while scanning address ")
			return &pb.CustomerInfo{}, err
		}
		customer.Addresses = append(customer.Addresses, &address)
	}
	return &customer, nil
}

// UpdateCustomer ...
func (r *customerRepo) UpdateCustomer(req *pb.CustomerUp) (*pb.CustomerResponse, error) {
	user := pb.CustomerResponse{}
	_, err := r.db.Exec(`update customers set updated_at=NOW(),
	first_name=$1,
	last_name=$2,
	bio=$3, email=$4,
	phone_number=$5 where id=$6`, req.FirstName, req.LastName, req.Bio, req.Email, req.PhoneNumber, req.Id)
	if err != nil {
		fmt.Println("error while updating customers")
		return &pb.CustomerResponse{}, err
	}

	for _, address := range req.Addresses {
		_, err := r.db.Exec(`update addresses set country=$1, street=$2 where id=$3`, address.Country, address.Street, address.Id)
		if err != nil {
			fmt.Println("error while update customer address")
			return &pb.CustomerResponse{}, err
		}
	}
	err = r.db.QueryRow(`select 
	id, first_name, 
	last_name, bio, email, 
	phone_number, created_at, updated_at from customers where id=$1`, req.Id).Scan(
		&user.Id, &user.FirstName, &user.LastName, &user.Bio, &user.Email, &user.PhoneNumber, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		fmt.Println("error while getting customers update")
		return &pb.CustomerResponse{}, err
	}
	rowsAddress, err := r.db.Query(`select id, owner_id, country, street from addresses where owner_id=$1`, req.Id)
	if err != nil {
		fmt.Println("error while getting customers addresses update")
		return &pb.CustomerResponse{}, err
	}
	defer rowsAddress.Close()

	for rowsAddress.Next() {
		addressRes := pb.Address{}
		err = rowsAddress.Scan(&addressRes.Id, &addressRes.OwnerId, &addressRes.Country, &addressRes.Street)
		if err != nil {
			fmt.Println("error while scanning customer addresses update")
			return &pb.CustomerResponse{}, err
		}
		user.Addresses = append(user.Addresses, &addressRes)
	}

	return &user, nil
}

// DeleteCustomer ...
func (r *customerRepo) DeleteCustomer(req *pb.CustomerID) (*pb.Empty, error) {
	usersRepo := pb.Empty{}
	err := r.db.QueryRow(`update customers set deleted_at=NOW() where id=$1 and deleted_at is null`, req.Id).Err()
	if err != nil {
		fmt.Println("error while deleting customers")
		return &pb.Empty{}, err
	}
	return &usersRepo, nil
}
